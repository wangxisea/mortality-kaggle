## Mortality data analysis
- Ex1 and Ex3 are written in Zeppelin note, which is in the JSON file. The reason is Zeppelin rather then *.py scripts was to accommodate the needs of plotting.
- Ex2 is written in mortality.py file, an example call could be: 
```python
spark-submit mortality.py --year 2010 --legend_file 2010_code.json --output_path /tmp/mortality/outpu
```
- Ex2 codes require python 3.6+, due to f-string implemented.

- Bonus Ex is addressed as following:
1. There a `convert_csv_to_parquet` function in mortality.py, which will convert data files into parquet and partitioned by its year.
2. In the solution of Ex2, data is read in format of parquet and with partition in arguments, by looping through the required year, Spark application will only read the relevant data, following transformations of TopN and sorting will be done accordingly.
3. Applying appropriate partitions on data and using Spark will allow the system scaling horizontally.
