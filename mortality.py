import toolz
import json
import argparse
from pyspark.sql import SparkSession, DataFrame
import pyspark.sql.functions as F
from pyspark.sql.window import Window
from itertools import chain


class mortality:
    def __init__(self, spark, args):
        self.spark = spark
        self.args = args
        self.ROOT_DIR = "/tmp/mortality"
        self.CSV_DIR = "/tmp/mortality/csv"
        self.LEGEND_DIR = "/tmp/mortality/legend"
        self.DATA_DIR = "/tmp/mortality/data"
        self.legend = self.load_legend()

    def convert_csv_parquet(self, year: str) -> None:
        df = self.spark.read.csv(f"{self.CSV_DIR}/{year}_data.csv", header=True)
        df = df.repartition(5)
        df.write.save(f"{self.DATA_DIR}/year={year}", mode="overwrite")

    @toolz.curry
    def df_to_csv(self, path: str, data: DataFrame) -> None:
        df = data.repartition(1)
        df.write.csv(path, mode="overwrite", header=True)

    def read_parquet_by_partition_year(self, year: list) -> DataFrame:
        df = self.spark.read.parquet(f"{self.DATA_DIR}/year={year}")
        return df

    @toolz.curry
    def select_rename_cols_count(self, data: DataFrame) -> DataFrame:
        new_cols = ["gender", "age_group", "cause"]
        df2 = data.select(["sex", "age_recode_12", "39_cause_recode"])
        df2 = df2.toDF(*new_cols)
        df2 = df2.groupBy(new_cols).count()
        return df2

    @toolz.curry
    def calc_top5_causes(self, data: DataFrame) -> DataFrame:
        window = Window.partitionBy(
            [F.col(c) for c in ["gender", "age_group"]]
        ).orderBy(data["count"].desc())
        df2 = data.select("*", F.rank().over(window).alias("rank")).filter(
            F.col("rank") <= 5
        )
        df2 = df2.orderBy(*["gender", "age_group", "cause"], F.col("count").desc())
        df2 = df2.drop("rank")
        return df2

    def load_legend(self) -> dict:
        legend_file = f"{self.LEGEND_DIR}/{self.args.legend_file}"
        with open(legend_file) as f:
            legend = json.load(f)
        return legend

    @toolz.curry
    def add_label_to_code(
        self, new_col: str, old_col: str, origin_col: str, data: DataFrame
    ) -> DataFrame:
        # TODO: create mapping table rather if..else in a config file to be more elegant
        mapping_expr = F.create_map(
            [F.lit(x) for x in chain(*self.legend.get(origin_col).items())]
        )
        if origin_col == "39_cause_recode":
            data = data.withColumn(old_col, F.concat(F.lit(0), F.col(old_col)))

        df2 = data.withColumn(new_col, mapping_expr.getItem(F.col(old_col)))
        return df2

    def main(self):
        return toolz.pipe(
            self.read_parquet_by_partition_year(self.args.year),
            self.select_rename_cols_count(),
            self.calc_top5_causes(),
            self.add_label_to_code("age_group_label", "age_group", "age_recode_12"),
            self.add_label_to_code("cause_label", "cause", "39_cause_recode"),
            self.df_to_csv(self.args.output_path),
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--year")
    parser.add_argument("--legend_file")
    parser.add_argument("--output_path")
    args = parser.parse_args()

    spark = SparkSession.builder.getOrCreate()

    m = mortality(spark, args)
    m.main()

"""
spark-submit mortality.py --year 2010 --legend_file 2010_code.json --output_path /tmp/mortality/output
"""
